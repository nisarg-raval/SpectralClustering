import numpy as np
import copy
from numpy import linalg as la

#generate a random graph with n nodes and k cluster
#return adjacency matrix of the graph
#TODO: deal with k clusters
def generateGraph(n,k):

	# # generate a random graph with n nodes
	# R = np.random.randint(2,size=(n,n))

	# # get symmetric matrix for adjacency matrix
	# G = (R + R.T)/2

	# #make the diagonal entries 0
	# np.fill_diagonal(G,0)

	G = np.empty((5,5))
	G[0,:] = [0,1,1,0,0]
	G[1,:] = [1,0,1,0,0]
	G[2,:] = [1,1,0,0,0]
	G[3,:] = [0,0,0,0,1]
	G[4,:] = [0,0,0,1,0]

	return G

#randomly divide nodes (columns) of G into two sets A and B
def splitGraph(G):

	#repeat until we get non-empty splits
	split = True
	while split:
		a = []
		b = []
		n = G.shape[0]
		for i in range(0,n):
			if np.random.random() < 0.5:
				a.append(G[:,i])
			else:
				b.append(G[:,i])

		if(len(a) > 0 and len(b) > 0):
			split = False

	#convert list into arrays and return
	A = np.array(a)
	B = np.array(b)

	#take transpose for representing as set of columns
	return [A.T, B.T]

def getProjection(M,tau,k):
	# print 'getProjection'
	# print M

	#get number of nodes in a graph
	n = M.shape[0]

	#initialize project matrix to zeros
	C = np.zeros((n,k))
	# print 'init C'
	# print C

	#to save nodes corresponding to each of the k projection vectors
	Cn = []

	#compute projection matrix of M.T (transpose)
	P = np.dot(M.T,M)
	# print 'P'
	# print P

	#classify every node into one of the k classes

	#u is a list of uncalssified nodes
	#initially all nodes are uncalssified
	U = range(n)
	for i in range(0,k):

		#check if there exist at least one unclassified node
		#TODO: what if all nodes are classified in fewer than k iterations?
		l = len(U)
		if(l < 1):
			break

		# print 'i:' + str(i)

		#select a random uncalssified node
		r = np.random.randint(0,l)
		u = U[r]
		Cn.append(u)
		# print 'u: ' + str(u)

		#get the uth column of transpose of M
		#vector of roughly n/2 length
		MTu = M.T[:,u]
		# print 'MTu'
		# print MTu
		for v in range(0,n):
			# print 'v: ' + str(v)

			#get the vth column of transpose of M
			MTv = M.T[:,v]
			# print 'MTv'
			# print MTv

			#project the difference on to P
			proj = np.dot(P,(MTu - MTv))

			# print 'proj'
			# print proj
			#if the norm of projection vector 
			#is smaller than the threshold tau 
			#assign v to be part of u's cluster
			# print la.norm(proj)
			if la.norm(proj) <= tau:
				# print 'adding ' + str(v) + ' to ' + str(i)
				C[v,i] = 1

				#mark v as classified and 
				#remove it from the list of unclassified nodes
				if v in U:
					U.remove(v)
		# print 'C'
		# print C
		# print 'U'
		# print U

	#process remaining unclassified nodes
	for u in U:
		closest_dist = 100000
		closest_cluster = 0
		MTu = M.T[:,u]
		for i in range(0,len(Cn)):
			v = Cn[i]
			MTv = M.T[:,v]
			proj = np.dot(P,(MTu - MTv))
			dist = la.norm(proj)
			if dist < closest_dist:
				closest_dist = dist
				closest_cluster = i
		C[u,closest_cluster] = 1
	
	# print 'Final C'
	# print C			
	PC = np.dot(C,C.T)	
	return PC


def partitionGraph(G,tau,k):

	#get number of nodes in the graph
	n = G.shape[0]
	# print n

	#split the nodes into two sets A and B
	[A, B] = splitGraph(G)
	# print 'A'
	# print A
	# print 'B'
	# print B

	#get the projection matrix for A and B
	P1 = getProjection(B,tau,k)
	# print 'P1'
	# print P1

	P2 = getProjection(A,tau,k)
	# print 'P2'
	# print P2

	#apply the projection on A and B
	H1 = np.dot(P1,A)
	# print 'H1'
	# print H1
	H2 = np.dot(P2,B)
	# print 'H2'
	# print H2

	# #TODO: check this
	H = np.hstack((H1,H2))
	# print 'H'
	# print H

	#partitioned the nodes
	#psi is the partitioned represented as a mapping from nodes to clusters
	psi = {}

	#u is a list of unpartitioned nodes
	#initially all nodes are unpartitioned
	U = range(n)

	#continue until there exist unpartitioned nodes
	while len(U) > 0:

		# print 'U len: ' + str(len(U))
		# print U

		#select a random unpartitioned node
		r = np.random.randint(0,len(U))
		u = U[r]
		# print 'u:' + str(u)

		#assign all nodes closest to u as a part of partition represented by u
		for v in range(0,n):
			# print 'v:' + str(v)
			dist = la.norm(H[:,u] - H[:,v])
			# print 'dist: of ' + str(u) + ' and ' + str(v) + ' :' + str(dist)
			if dist <= tau:
				psi[v] = u

				#remove the node from list of unpartitioned nodes
				if v in U:
					# print 'removing node: ' + str(v)
					U.remove(v)

	return psi

if __name__ == "__main__":
	
	#similarity threshold tau
	tau = 1

	#number of nodes in a graph
	n = 5

	#communities (clusters) in a graph
	k = 2

	G = generateGraph(n,k)
	print 'G'
	print G

	psi = partitionGraph(G,tau,k)
	# print 'psi'
	# print psi

	clusters = {}
	for key,value in psi.iteritems():
		if clusters.has_key(value):
			clusters[value].append(key)
		else:
			clusters[value] = [key]

	print 'clusters'
	for key,value in clusters.items():
		print key,value
