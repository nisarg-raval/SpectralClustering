import math
import random
import numpy as np
from numpy import linalg as la


def split_matrix(G, n):
  # random division of {1, ..., n} as [A | B]
  split = True
  while split:
    a = []
    a_index = []
    b = []
    b_index = []
    for i in xrange(n):
      if bool(random.getrandbits(1)):
        a.append(G[:,i])
        a_index.append(i)
      else:
        b.append(G[:,i])
        b_index.append(i)
    
    if len(a) > 0 and len(b) > 0:
      split = False

  A = np.array(a).T
  B = np.array(b).T
  return [(a_index, A), (b_index, B)]

# k = 2, a bipartition
def comb_proj(A, s_m, tau):
  n = A.shape[0]
  l = A.shape[1]

  U = range(l) # unassigned vertices
  T = []

  while len(U) > s_m / 2 or not T:
    # choose v_i uniformly at random from U
    v_i = random.choice(U)

    # T_i = vertices within tau (sort of) of v_i
    T_i = []
    T_i.append(v_i) # first element of T_i is v_i

    # P_AT = projection matrix onto A^T
    AT_orth = la.qr(A.T)[0]
    P_AT = np.dot(AT_orth, AT_orth.T)

    # assign closest
    for j in U:
      if j != v_i and (la.norm(np.dot(P_AT, A.T[:,v_i] - A.T[:,j]))) <= tau:
        T_i.append(j)

    # mark as assigned
    for j in T_i:
      U.remove(j)

    T.append(T_i)

  # assign the remaining unassigned nodes
  for j in U:
    best_T_i = None
    dist = float("inf")
    for T_i in T:
      v_i = T_i[0]
      dist_i = la.norm(np.dot(P_AT, A.T[:,v_i] - A.T[:,j]))
      if dist_i < dist:
        best_T_i = T_i
        dist = dist_i
    if best_T_i is None:
      print("ERROR: extra vertex assigned to nothing")
      return None
    best_T_i.append(j)

  # construct characteristic vectors of all T_i
  c = []
  for T_i in T:
    c_i = np.zeros(n)
    for j in T_i:
      c_i[j] = 1
    c.append(c_i)

  # construct the output projection matrix
  C = np.empty((n, len(c)))
  for i in xrange(len(c)):
    C[:,i] = c[i]
  C_orth = la.qr(C)[0]
  P = np.dot(C_orth, C_orth.T)

  return P

# 2-partition of a graph G, with threshold t
def partition(G, s_m, tau):
  n = G.shape[0]
  if n != G.shape[1]:
    print("Adjacency matrix is not square!")
    return None
  if n == 0:
    print("Dimension 0")
    return None

  split_G = split_matrix(G, n)
  # a, b are the indices into the original G;
  # need them to map back the final partitioning
  a, A = split_G[0]
  b, B = split_G[1]

  # P1 = CProj(B), P2 = CProj(A)
  P1 = comb_proj(B, s_m, tau)
  P2 = comb_proj(A, s_m, tau)

  # H = [P1(A) | P2(B)]
  P1A = np.dot(P1, A)
  P2B = np.dot(P2, B)
  H = np.hstack((P1A, P2B))
  H_indices = a + b


  # NOTE
  # from this point forward is (4) of Partition() in the paper
  # where we assign each vertex a label
  # deviates slightly using comments from:
  #     http://arxiv.org/pdf/1404.3918.pdf
  # notably does not involve tau, but is still greedy

  # longest edge
  longest = None
  longest_dist = float("-inf")

  # pairwise distances
  H_dists = np.empty((n, n))
  for i in xrange(n):
    for j in xrange(n):
      H_dists[i,j] = la.norm(H[:,i] - H[:,j])
      if H_dists[i,j] > longest_dist:
        longest_dist = H_dists[i,j]
        longest = (i, j)

  #print "H_dists"
  #print H_dists

  # partition by connected components
  # output[v] is the label of node v \in G
  output = [-1 for x in xrange(n)]

  # label the 2 furthest separated
  longest_i = longest[0]
  longest_j = longest[1]
  output[H_indices[longest_i]] = 0
  output[H_indices[longest_j]] = 1

  # greedily assign all other points
  U = range(n)
  U.remove(longest_i)
  U.remove(longest_j)
  for i in U:
    if H_dists[i,longest_i] < H_dists[i,longest_j]:
      output[H_indices[i]] = 0
    else:
      output[H_indices[i]] = 1

  return output


# test procedures

# two disjoint complete graphs of size n
def perfect_partition(n):
  G = np.zeros((2 * n, 2 * n))
  G[0:n,0:n] = 1
  G[n:2*n,n:2*n] = 1
  return G


# main
if __name__ == "__main__":
  G = perfect_partition(10)
  # TODO choose these
  s_m = 4
  tau = 2.5
  print partition(G, s_m, tau)
